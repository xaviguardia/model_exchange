const path = require('path');
const dirName=path.join(__dirname,"files");
console.log(dirName);
const unzip = require('unzip');
const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const fs = require('fs');
var lastUpdate = new Date();
// default options
app.use(fileUpload());
app.use('/'+dirName,express.static(dirName));
var dir = dirName;
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
app.post('/upload', function(req, res) {
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    var sampleFile = req.files.sampleFile;
    var fileName = sampleFile.name.substr(0, sampleFile.name.lastIndexOf('.'));
    var extension = sampleFile.name.substr(sampleFile.name.lastIndexOf('.')+1 , sampleFile.length);
    // Use the mv() method to place the file somewhere on your server
    var newFile = dirName+'/'+sampleFile.name;
    sampleFile.mv(newFile, function(err) {
        if (err)
            return res.status(500).send(err);
        lastUpdate= new Date();
        res.send('File uploaded!');
        if(extension.toLowerCase() == 'zip') {
            console.log('file uploaded & unzipped');
            fs.createReadStream(newFile).pipe(unzip.Extract({ path: dirName }));
        }
    });
});

// 1487937188535
// 1487937215428

// 192.168.1.108

app.get("/", function(req,res){
    res.sendfile(path.join(__dirname,'form.html'));
});

app.get("/lastfile", function(req,res) {
    res.send(""+lastUpdate.getTime());
    console.log("Hololens checking");
});

app.get("/dir", function(req,res) {
    console.log("files");
    const testFolder = './files';

    var files = fs.readdirSync(testFolder);

        console.log(files);
        files.forEach( function(file) {
            console.log(file);

        });
     res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(files));
});
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
